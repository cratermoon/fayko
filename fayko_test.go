package fayko_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cratermoon/fayko"
)

func TestRandomInt(t *testing.T) {
	max := 1024
	for i := 1; i < max; i++ {
		n := fayko.RandomInt(i)
		assert.LessOrEqual(t, n, int64(max))
	}
}

func TestRandomAirportCode(t *testing.T) {
	c := fayko.AirportCode()
	assert.Equal(t, 3, len(c)
}

func TestRandomName(t *testing.T) {
	maxlength := 255
	for i := 1; i < maxlength; i++ {
		name := fayko.RandomName(i)
		assert.NotEqual(t, "BADSTRING", name)
		assert.NotEqual(t, 0, len(name))
		assert.LessOrEqual(t, 1, len(name), len(name))
		assert.LessOrEqual(t, len(name), i, len(name), i)
	}
}

func TestRandomUserID(t *testing.T) {
	var idTests = []struct {
		prefix string
		sep    string
	}{
		{"faykotest", "|"},
		{"aaaa", "%"},
		{"1", "=="},
	}
	for _, tt := range idTests {
		id := fayko.RandomUserID(tt.prefix, tt.sep)
		assert.Contains(t, id, tt.prefix, "missing", tt.prefix)
		assert.Contains(t, id, tt.sep, "missing", tt.sep)
	}
}
