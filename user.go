package fayko

import "strings"

// RandomUserID generates a random userid-looking string with the given
// prefix and sep prepended
func RandomUserID(prefix, sep string) string {
	var builder strings.Builder
	builder.WriteString(prefix)
	builder.WriteString(sep)
	builder.WriteString(RandomString(16))
	return builder.String()
}
