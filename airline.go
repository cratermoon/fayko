package fayko


var airportcodes []string


func AirportCode() string {
	return randElt(airportcodes)
}

func init() {
	airportcodes = mustRead(datadir + "/AIRPORTCODES.TXT")
}
