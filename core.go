package fayko

import (
	"bufio"
	"crypto/rand"
	"fmt"
	"io"
	"math/big"
	"os"
	"regexp"
	"strings"
)

const datadir = "/usr/local/share/fayko/data"

var surnames []string
var biznames []string
var givenNames []string
var domains []string

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// RandomString returns a hex string exactly len bytes long
func RandomString(len int) string {
	size := len / 2
	bits := make([]byte, size)
	n, err := io.ReadFull(rand.Reader, bits)
	if n != size || err != nil {
		return "BADSTRING"
	}
	return fmt.Sprintf("%x", bits)
}

func randElt(s []string) string {
	return s[int(RandomInt(len(s)))]
}

var nonalphanum = regexp.MustCompile("[^a-zA-Z0-9]+")

func clean(s string) string {
	return nonalphanum.ReplaceAllString(s, "")
}

// BizName returns a string randomly select from the list of business
// names, converted to lower case and with all non-alphanum and
// whitespace removed
func BizName() string {
	return clean(strings.ReplaceAll(strings.ToLower(randElt(biznames)), " ", ""))
}

// DomainName returns a domain randomly selected from the official TLD list
func DomainName() string {
	return strings.ToLower(randElt(domains))
}

// RandomSurname returns surname (family name) randomly selected from
// the list of surnames
func RandomSurname() string {
	return randElt(surnames)
}

// RandomGivenName returns name (first name) randomly selected from
// the list of names
func RandomGivenName() string {
	return randElt(givenNames)
}

// RandomName returns a hex string of random length in [1,maxLen)
func RandomName(maxLen int) string {
	size := int(RandomInt(maxLen))
	if size == 0 {
		size = 1
	}
	bits := make([]byte, size)
	n, err := io.ReadFull(rand.Reader, bits)
	if n != size || err != nil {
		return "BADSTRING"
	}
	s := fmt.Sprintf("%x", bits)
	return s[:size]
}

const UpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const Specials = "~!@#$%^&*-+"
const Digits = "0123456789"

// RandomInt returns a uniform random int64 in [0, size). It panics if size <= 0.
func RandomInt(size int) int64 {
	i, _ := rand.Int(rand.Reader, big.NewInt(int64(size)))
	return i.Int64()
}

// RandomUpperCase returns a randomly selected ASCII character in the A-Z range
func RandomUpperCase() string {
	return string(UpperCase[RandomInt(len(UpperCase))])
}

// RandomSpecial returns a randomly selected ASCII character from the Specials list
func RandomSpecial() string {
	return string(Specials[RandomInt(len(Specials))])
}

// RandomDigit returns a randomly selected digit from 0-9 as a string
func RandomDigit() string {
	return string(Digits[RandomInt(len(Digits))])
}

// Password returns a string of length size + 3 of the form
// UpperCase + random string + Special + Digit
//
// this isn't really intended to generate strong passwords, but if size
// is chosen appropriately the result will be difficult to crack even
// for an attacker knowing the pattern
func Password(size int) string {
	sb := strings.Builder{}
	sb.Grow(size + 3)
	upper := RandomUpperCase()
	special := RandomSpecial()
	number := RandomDigit()
	fmt.Fprintf(&sb, "%s%s%s%s", upper, RandomString(size), special, number)
	return sb.String()
}

func mustRead(fname string) []string {
	lines, err := readLines(fname)
	if err != nil {
		panic(err)
	}
	return lines
}

func init() {
	surnames = mustRead(datadir + "/NAMES.TXT")
	givenNames = append(mustRead(datadir+"/NAMES-F.TXT"), mustRead(datadir+"/NAMES-M.TXT")...)
	domains = mustRead(datadir + "/tlds-alpha-by-domain.txt")
	biznames = mustRead(datadir + "/business-names.txt")
}
